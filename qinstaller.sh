#!/usr/local/bin/bash

#LWQ qmail kurulum scripti.
#Author: Cagri Ersen
#cagri.ersen@gmail.com
#Surum: 0.4 (16.03.2013)

##############################################################
#                                                            #
#  KURULUMA BASLAMADAN ONCE MUTLAKA "README"'YI OKUYUNIZ!    #
#                                                            #
##############################################################

function OnayAL() {
while true
do
echo -n "[Devam etmek istiyor musunuz ? <Evet/Hayir>] :"
read ONAY
case $ONAY in
Evet|evet|EVET) break ;;
Hayir|HAYIR|hayir|Hay\xc4\xb1r)
echo $ONAY dediginiz icin kurulum sonlandiriliyor...
exit
;;
*) echo Evet ya da Hayir demelisiniz.
esac
done
echo $ONAY dediniz, kuruluma devam ediyoruz.
}


  echo "" && echo ""
  echo "****************************************************************"
  echo "                    ROOT USER KONTROLU" && echo ""
  echo "  Bu script ile qmail kurulumu yapabilmek icin sisteme root"
  echo "kullanicisi ile login olmaniz gerekir. Once bu kontrol edilecek!"
  echo "****************************************************************"
  echo ""

echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"


	read cevap;

	echo ""

		sleep 1

	if [ `whoami` != "root" ]

 	then

		echo "Bu kurulum scriptini root olarak calistirabilirsiniz."
		echo "Lutfen root kullanicisi ile login olun." 1>&2
		echo "Kurulum sonlandiriliyor..." && echo ""

	sleep 1

	exit 0
	
	else

		 echo "Root olarak login durumdasiniz."
		 echo "Bu durumda kuruluma devam edebiliriz."

	fi
	
	sleep 1
	
  echo ""
  echo "***************************************************************************"
  echo "                SENDMAILIN DEVRE DISI BIRAKILMASI" && echo ""
  echo "  Kuruluma baslamadan once sendmail durdurulacak ve startuptan kaldirilacak"
  echo ""
  echo "***************************************************************************"
  echo ""

		echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"

		read cevap;

		echo ""

		sleep 1

			echo "Sendmail durduruluyor...." && echo ""
			
		 	sh /etc/rc.d/sendmail stop
		sleep 1

 			echo "" && echo "Done!" && echo ""

                sleep 1

			echo "sendmail startuptan kaldiriliyor..." && echo ""

        more /etc/rc.conf |grep 'sendmail_enable="NO"'

                if [ $? = 0 ]

                then

		sleep 1

                echo "sendmail zaten disable edilmis durumda..." && echo ""

		sleep 1

                else
                        echo 'sendmail_enable="NO"' >> /etc/rc.conf
                        echo 'sendmail_submit_enable="NO"' >> /etc/rc.conf
                        echo 'sendmail_outbound_enable="NO"' >> /etc/rc.conf
                        echo 'sendmail_msp_queue_enable="NO"' >> /etc/rc.conf

                sleep 1

                        echo "Done!" && echo ""


                fi


	sleep 1

  echo ""
  echo "***************************************************************************"
  echo "                            DOWNLOADS" && echo ""
  echo "  Simdiki adimda root (/) dizini altinda (eger yoksa) qmail-downloads"
  echo "isimli bir klasor olusturacak ve gerekli paketler bu dizine download"
  echo "edilecek!. "
  echo ""
  echo "***************************************************************************"
  echo ""

                echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"

                read cevap;

                echo ""

                sleep 1

			  if [ -d /qmail-downloads ]

			  then

				          echo "/qmail-downloads dizini mevcut. Dosyalar bu dizine download edilecek!" && echo ""

			  else

          				echo "/qmail-downloads dizini mevcut degil!" && echo ""

 				   mkdir /qmail-downloads

 			 sleep 1 

          				echo "Olusturuldu!" && echo ""

 			 fi

	sleep 1

        echo "Paketler download ediliyor..." && echo ""

        cd /qmail-downloads

                        sleep 1

	fetch -q http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz 


                                echo "netqmail-1.06.tar.gz ........ done!"

                        sleep 1 

        fetch -q http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz


                                echo "ucspi-tcp-0.88.tar.gz ........ done!"

                        sleep 1 


        fetch -q http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz


                        echo "daemontools-0.76.tar.gz ........ done!" && echo ""

       		      sleep 1 

        echo ""
        echo "*********************************************************************"
        echo "              PAKETLERIN CHECK EDILMESI!" && echo ""
        echo "  Download islemleri tamamlanmis gorunuyor..." && echo ""
        echo "  Ancak herhangi bir baglanti sorunu yasar ve paketler duzgun"
        echo "olarak download edilemezse ilerki adimlarda kurulum scripti"
        echo "hata verecektir. Bu nedenle bu adimda download isleminin sorunsuz"
        echo "gittigindan emin olmak icin paketlerin, /qmail-downloads dizininde"
        echo "olup olmadigi ve sizelarinin dogrulugu kontrol edilecek!"
        echo "********************************************************************"
        echo ""

                echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"

                read cevap;

                echo ""

	sleep 1

        echo "Paketler /qmail-downloads dizininde mevcut mu ?" && echo ""

        sleep 1


        if [ -f /qmail-downloads/daemontools-0.76.tar.gz ]

        then

                echo "Check daemontools ........ Ok!"

        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "DAEMONTOOLS paketi mevcut degil, bu durumda kuruluma devam edemiyoruz."
                echo "Lutfen http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz adresine,"
                echo "erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                sleep 1

                exit 0
      fi        

        sleep 1


        if [ -f /qmail-downloads/ucspi-tcp-0.88.tar.gz ]

        then

                echo "Check ucspi ........ Ok!"

        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "UCSPI paketi mevcut degil, bu durumda kuruluma devam edemiyoruz."
                echo "Lutfen http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz "
                echo " adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini"
                echo "yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                sleep 1

                exit 0

        fi


        sleep 1


        if [ -f /qmail-downloads/netqmail-1.06.tar.gz ]

        then

                echo "Check netqmail ........ Ok!"
                echo ""

        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "NETQMAIL paketi mevcut degil, bu durumda kuruluma devam edemiyoruz."
                echo "Lutfen http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz "
                echo " adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini"
                echo "yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                sleep 1

	sleep 1

	exit 0
 
      fi

	sleep 1
        
	echo "Dosyalarin ebatlari dogru mu ?"
        echo ""


        sleep 1

        DTNAME=/qmail-downloads/daemontools-0.76.tar.gz
        eval $(stat -s "$DTNAME")

        if [ ${st_size} = 36975 ]

        then

                echo "daemontools-0.76.tar.gz ...... Ok! (${st_size} bytes)" 


        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "daemontools-0.76.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam"
                echo "edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir."
                echo "Lutfen http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz "
                echo "adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini"
                echo "yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                sleep 1


                exit 0

        fi

        sleep 1

        UCNAME=/qmail-downloads/ucspi-tcp-0.88.tar.gz
	eval $(stat -s "$UCNAME")

        if [ ${st_size} = 53019 ]

        then

                echo "ucspi-tcp-0.88.tar.gz ...... Ok! (${st_size} bytes)"



        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "ucspi-tcp-0.88.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam"
                echo "edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir."
                echo "Lutfen http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz "
                echo "adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini"
                echo "yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                sleep 1

                exit 0

        fi

        sleep 1

        NQNAME=/qmail-downloads/netqmail-1.06.tar.gz
	eval $(stat -s "$NQNAME")

        if [ ${st_size} = 260941 ]

        then

                echo "netqmail-1.06.tar.gz ...... Ok! (${st_size} bytes)"


        else

                echo ""
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                echo "netqmail-1.06.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam"
                echo "edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir."
                echo "Lutfen Lutfen http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz "
                echo "adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini"
                echo "yeniden calistiriniz."
                echo "" && echo "Kurulum sonlandiriliyor..."
                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                sleep 1

                exit 0

        fi

	sleep 1

	echo ""
        echo "************************************************************************"
        echo "                        UNCOMPRESS ISLEMLERI" && echo ""
        echo "  Simdi download edilen paketler derleme isleminin yapilacagi dizinlere"
        echo "tasinacak ve uncompress edilecek."
        echo ""
        echo "netqmail-1.06 ve ucspi-tcp-0.88 paketleri usr/local/src dizinine,"
        echo "daemontools-0.76.tar.gz ise /package dizinine tasinacak. "
        echo "Eger ilgili dizinler sistemde bulunamazsa otomatik olarak olusturulacak."
        echo "************************************************************************"
        echo ""


echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"


        read cevap;

        echo ""

                sleep 1

        if [ -d /usr/local/src ]

        then

                echo "" && echo "/usr/local/src dizini mevcut. Dosyalar bu dizine tasinacak!"
		echo ""

        else

                echo "/usr/local/src dizini mevcut degil! Olusturuluyor..."
                echo ""

                mkdir -p /usr/local/src

                sleep 1

                echo "Done!"
                echo ""

        fi

        sleep 1

        echo "netqmail-1.06.tar.gz ve ucspi-tcp-0.88.tar.gz dosyalari dizine tasiniyor..." && echo ""

        cd /qmail-downloads

        mv -f netqmail-1.06.tar.gz ucspi-tcp-0.88.tar.gz /usr/local/src

        sleep 1




        if [ -d /package ]

        then

                echo ""
                echo "/package dizini mevcut. Dosyalar bu dizine tasinacak!"

        else

                echo "/package dizini mevcut degil. Olusturuluyor..."
                echo ""

                mkdir -p /package

                sleep 1

                echo "Done!"

        fi

        sleep 1

        echo ""
        echo "daemontools-0.76.tar.gz ve ucspi-tcp-0.88.tar.gz dosyalari dizine tasiniyor..."
        echo ""

        cd /qmail-downloads

        mv -f daemontools-0.76.tar.gz /package

        sleep 1

        echo "Dizin permission'i 1755'e set ediliyor... "
        echo ""

        chmod 1755 /package

        sleep 1

        echo "netqmail-1.06.tar.gz paketi uncompress ediliyor... "

        sleep 1

        cd /usr/local/src
        tar -xf netqmail-1.06.tar.gz
        rm -f netqmail-1.06.tar.gz

        sleep 1
        echo "Done!"
        echo ""

        sleep 1

        echo "ucspi-tcp-0.88.tar.gz paketi uncompress ediliyor... "

        sleep 1

        tar -xf ucspi-tcp-0.88.tar.gz
        rm -f ucspi-tcp-0.88.tar.gz

        sleep 1
        echo "Done!"
        echo ""

        sleep 1

        echo "daemontools-0.76.tar.gz paketi uncompress ediliyor... "

        sleep 1

        cd /package
        tar -xf daemontools-0.76.tar.gz
        rm -f daemontools-0.76.tar.gz

        echo "Done!"
        echo ""
        echo ""


        sleep 2

        echo "******************************************************************************"
        echo "               USER VE GROUPLARIN OLUSTURULMASI"
        echo ""
        echo "  Bu adimda, qmail dizinleri ve userlari sisteme eklenecek."
        echo ""
        echo "NOT: Daha onceden qmail kurulumu yaptiysaniz ya da bu scripti birden fazla "
        echo "kez calistirdiysaniz, /var/qmail dizini ve ilgili kullanicilar sistemde"
        echo "sistemde mevcut durumda olabilirler!" && echo ""
        echo "Bu nedenle script tarafindan once /var/qmail dizininin var olup olmadigi ve"
        echo "ilgili kullanicilarin sisteme ekli olup olmadigi kontrol edilecek."
        echo "Boyle bir durumda, onceki kuruluma zarar vermemek icin bir uyari verilerek"
        echo "kurulum sonlandirilacaktir."
        echo "Eger bu yonde bir durum yasarsaniz, uyarida belirtilen adimlari uyguladiktan"
        echo "sonra bu scripti yeniden calistiriniz."
        echo "******************************************************************************"
        echo ""

        sleep 1

OnayAL

                                      echo "" && echo "Daha onceki qmail kurulumlari kontrol ediliyor!"
                                      echo ""
                                sleep 1

                                        echo "Sistemde /var/qmail dizini var mi ?" && echo ""

                                sleep 1

                                        if [ -d /var/qmail ]

                                        then
                                                echo ""
                                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                                                echo "Ooopss..." && echo "/var/qmail dizini mevcut!" && echo ""
                                                echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                                                echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                                                echo "olabilirsiniz..." && echo ""
                                                echo " BU DURUMDA YAPABILECEKLERINIZ: "
                                                echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                                                echo "devam edebilmek icin qmail'i manuel olarak kaldirmaniz gerekiyor."
                                                echo ""
                                                echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                                                echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                                                echo "uninstall.sh scriptini calistirarak kurulumu geri aliniz"
                                                echo ""
                                                echo "Varolan qmail'i sistemden kaldirdiktan sonra install-qmail.sh siptinini"
                                                echo "yeniden calistirabilirsiniz!"
                                                echo ""
                                                echo "Kurulum sonlandiriliyor..."
                                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                                        sleep 1

                                        exit 0

                                        else

                                              echo "Default qmail kurulumuna rastlanmadi!" && echo ""
                                              echo "Devam ediliyor..." && echo ""
			                        sleep 1

 			                       echo "/var/qmail dizini olusturuluyor!" && echo ""

 			                       mkdir /var/qmail
                       				 sleep 1

			                        echo "Done!" && echo ""

                                      fi

                        sleep 1

                        echo "Grup ve Kullanicilar sisteme ekleniyor!" && echo ""

                        sleep 2

		more /etc/group |grep 'nofiles:'

			if [ $? = 0 ]

		                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""nofiles" isimli grup sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else

                        pw groupadd nofiles && echo "pw groupadd: nofiles ........ done!"

        fi
	

		sleep 1

        more /etc/passwd |grep 'alias:' 

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""alias" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo "" 
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
			pw useradd alias -g nofiles -d /var/qmail/alias -s /nonexistent
                        echo "user add: alias ........ done!"

        fi

		sleep 1

        more /etc/passwd |grep 'qmaild:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmaild" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else	
			pw useradd qmaild -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmaild ........ done!"

        fi

		sleep 1

        more /etc/passwd |grep 'qmaill:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmaill" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
                        pw useradd qmaill -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmaill ........ done!"

        fi

	sleep 1	

        more /etc/passwd |grep 'qmailp:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmailp" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
                        pw useradd qmailp -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmailp ........ done!"

        fi

	sleep 1


                more /etc/group |grep 'qmail:'

                        if [ $? = 0 ]

                                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmail" isimli grup sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else

                        pw groupadd qmail && echo "pw groupadd: nofiles ........ done!"

        fi

	sleep 1

        more /etc/passwd |grep 'qmailq:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmailq" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
                        pw useradd qmailq -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmailq ........ done!"

        fi

	sleep 1 

        more /etc/passwd |grep 'qmailr:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmailr" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
                        pw useradd qmailr -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmailr ........ done!"

        fi

	sleep 1

        more /etc/passwd |grep 'qmails:'

        if [ $? = 0 ]

                then

                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        echo "Ooopss..." && echo ""
                        echo ""qmails" isimli kullanici sistemde mevcut.!" && echo ""
                        echo "Sisteminizde daha onceden qmail kurulmus olabilir ya da"
                        echo "bu scripti daha onceden calistirip qmail kurulumu yapmis"
                        echo "olabilirsiniz..." && echo ""
                        echo "BU DURUMDA YAPABILECEKLERINIZ: "
                        echo "1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz"
                        echo "devam edebilmek icin qmail'i manuel olarak kaldiriniz..."
                        echo ""
                        echo "2 - Eger bu scripti daha onceden calistirarak qmail kurulumu"
                        echo "gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan "
                        echo "uninstall.sh scriptini calistirarak, kurulumu geri aliniz!"
                        echo ""
                        echo "Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh "
                        echo "calistirabilirsiniz!"
                        echo ""
                        echo "Kurulum sonlandiriliyor..."
                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                        sleep 1

                        exit 0


                else
                        pw useradd qmails -g nofiles -d /var/qmail -s /nonexistent
                        echo "user add: qmails ........ done!"

        fi

	sleep 1


                        echo ""
                        echo "Tum kullanici ve gruplar sisteme eklendi!."
                        echo ""



sleep 2


        echo ""
        echo "*************************************************************************"
        echo "                   NETQMAIL KURULUMU                             "
        echo ""
        echo "  Simdi, netqmail-1.06 pakeri derlenerek sisteme kurulacak. qmail"
        echo "kurulumu sirasinda sunucunuzun tam alan adini girmeniz (FQDN) istenecek. "
        echo ""
        echo "Lutfen dogru girdiginize emin olun!"
        echo "*************************************************************************"
        echo ""

sleep 1

OnayAL
                        echo "netqmail kurulum islemi baslatiliyor..."
                        echo ""

			cd /usr/local/src/
			fetch http://www.syslogs.org/qmail/patches/dns-response-size.patch
			sleep 1
			fetch http://www.syslogs.org/qmail/patches/extra-patch-utmpx
			sleep 1
			fetch http://www.syslogs.org/qmail/patches/qmail-remote.log.patch
			sleep 1
                        cd /usr/local/src/netqmail-1.06
                        sleep 2
			
			patch < /usr/local/src/dns-response-size.patch
			sleep 2
			patch < /usr/local/src/extra-patch-utmpx
			sleep 2
			patch < /usr/local/src/qmail-remote.log.patch
                        
			sleep 2
			
			make setup check

                        sleep 2

                        echo "Derleme islemi tamamlandi!"
                        echo ""
                        sleep 1

                        echo "Installation baslatiliyor..."
                        echo ""
                        echo "UYARI: FQDN'i dogru girdiginize emin olunuz..."
                        echo ""
                        echo -n "Lutfen sunucunuzun tam alan adini (FQDN) yazin [ENTER]: "

                        read FQDN;
                        ./config-fast $FQDN

                echo ""
                sleep 1
                echo "Done!"
                echo ""
                sleep 1
                echo "Netqmail kurulumu tamamlandi!"
                echo ""

                sleep 2
        echo ""
        echo "************************************************************************"
        echo "                            UCSPI KURULUMU"
        echo " "
        echo "Bu adimda, ucspi-tcp-0.88 paketi compile edilerek sisteme yuklenecek."
        echo " "
        echo "************************************************************************"
        echo ""

 sleep 1

OnayAL
	                        cd /usr/local/src/ucspi-tcp-0.88
                        echo ""
                        echo "errno patch'i uygulaniyor..."
                        echo ""
                        sleep 1
                        patch < /usr/local/src/netqmail-1.06/other-patches/ucspi-tcp-0.88.errno.patch

                        sleep 2

                        echo "Done!"

                        sleep 2

                        echo ""
                        echo "make islemi baslatiliyor..."

                        sleep 2

                        make

                        sleep 1
                        echo ""
                        echo "Done!"

                        sleep 2

                        echo ""
                        echo "make setup check islemi baslatiliyor..."

                        sleep 2

                        make setup check

                        sleep 1
                        echo ""
                        echo "Done!"
                        echo ""
                        sleep 1
                        echo "Ucspi kurulumu tamamlandi!"
                        echo ""
                        sleep 1


        echo ""
        echo "*************************************************************************"
        echo "                    DAEMONTOOLS KURULUMU"
        echo " "
        echo "Bu adimda, daemontools-0.76 paketi compile edilerek sisteme yuklenecek."
        echo " "
        echo "*************************************************************************"
        echo ""


 sleep 1


OnayAL

                       cd /package/admin/daemontools-0.76
                        echo ""
                        echo "errno patch'i uygulaniyor..."
                        echo ""
                        sleep 1
                        cd src
                        patch < /usr/local/src/netqmail-1.06/other-patches/daemontools-0.76.errno.patch

                        sleep 2

                        echo "Done!"

                        cd /package/admin/daemontools-0.76

                        echo ""
                        echo "Kurulum baslatiliyor..."
                        echo ""
                        sleep 2

                        package/install

                        sleep 2

                        echo ""
                        echo "Done!"
                        echo ""
                        sleep 1
                        echo "Daemontools kurulumu tamamlandi!"
                        echo ""

        sleep 1

	echo "svscan baslatiliyor" && echo ""
	
	csh -cf '/command/svscanboot &'

	sleep 1

	echo "Done!" && echo ""

        sleep 1

        echo "************************************************************************"
        echo "                    KURULUM SONRASI ISLEMLERI" && echo ""
        echo "   Bu adimda, qmail'in startup configuration islemleri yapilacak."
        echo " qmail conf dosyalari download edilecegi icin, internet erisiminizin"
        echo " problemsiz olduguna emin olun. Aksi halde kurulum scripti hata"
        echo " verecektir." && echo ""
        echo "Ilgili conf dosyalari http://www.syslogs.org/qmail adresinde"
        echo "bulunmaktadir."
        echo "************************************************************************"
        echo ""

echo -n "[Devam etmek icin ENTER'a basin [Sonlandirmak icin <CTRL+C>]"

        read cevap;

        echo ""

                sleep 1

        echo ""
        echo "/var/qmail/rc dosyasi download ediliyor..."
        cd /var/qmail
        fetch -q http://www.syslogs.org/qmail/rc
        sleep 1
        echo "Done!"
        echo ""

        sleep 1

                                echo "rc betigi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "rc dosyasi yerinde mi ? "

                                if [ -f /var/qmail/rc ]

                        then
                                        sleep 1
                                        echo "rc .......... OK"

                        else
                                sleep 1
                                echo ""
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!"
                                echo "Ooopss..." && echo ""
                                echo "rc betigi mevcut degil, download sirasinda sorun olusmus olabilir!"
                                echo ""
                                echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                echo "scriptini yeniden calistirin." && echo ""
                                echo "1 - http://www.syslogs.org/qmail/rc adresine erisiminiz"
                                echo "oldugundan emin olun." && echo ""
                                echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                echo "qmail kurulumunu yeniden baslatin."
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                echo ""


                                sleep 1

                                echo "Kurulum sonlandiriliyor!"
                                exit 0

                        fi


                        sleep 1

                                        echo ""
                                        echo "rc betiginin ebati dogru mu ?"

                        rcNAME=/var/qmail/rc
                        eval $(stat -s "$rcNAME")

                        if [ ${st_size} = 215 ]

                        then

                                        sleep 1
                                        echo "OK. $rcNAME = (${st_size} bytes) "



                        else
                                        sleep 1

                                        echo ""
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "rc betiginin size'i dogrulanamadi! Download sirasinda bir sorun"
                                        echo "olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/rc adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                        echo ""

                                        sleep 1

                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        sleep 1

        echo ""
        echo "/var/qmail/rc permission'u 755'e set ediliyor..."
        chmod 755 /var/qmail/rc
        echo ""

        sleep 1


        echo "/var/qmail/log dizini olusturuluyor..."
        mkdir /var/log/qmail
        echo ""

        sleep 1

        echo "qmail ./Maildir yapisina gore set ediliyor..."
        echo ./Maildir >/var/qmail/control/defaultdelivery
        echo ""

        sleep 1

        echo "qmail startup scripti (qmailctl) download ediliyor..."
        cd /var/qmail/bin
        fetch -q http://www.syslogs.org/qmail/qmailctl
        sleep 1
        echo "Done!"
        echo ""

        sleep 1


                                echo "qmailctl betigi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "qmailctl dosyasi yerinde mi ? "


                                if [ -f /var/qmail/bin/qmailctl ]

                        then
                                        sleep 1
                                        echo "qmailctl .......... OK"

                        else
                                sleep 1
                                        echo ""
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmailctl betigi mevcut degil! Download sirasinda sorun olusmus"
                                        echo "olabilir. Bu durumda kuruluma devam edemiyoruz!" && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmailctl adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"

                                sleep 1

                                echo ""
                                sleep 1
                                echo "Kurulum sonlandiriliyor!"
                                exit 0

                        fi

                                        echo ""
                                        echo "qmailctl betiginin ebati dogru mu ?"

                                        sleep 1


                        ctlNAME=/var/qmail/bin/qmailctl
                        eval $(stat -s "$ctlNAME")

                        if [ ${st_size} =  3008 ]

                        then
                                        echo "qmailctl size ...... Ok! (${st_size} bytes)"



                        else
                                 sleep 1

                                        echo ""
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmailctl betiginin size'i dogrulanamadi! Download sirasinda bir"
                                        echo "sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmailctl adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"

                                 sleep 1

                                                echo ""
                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        sleep 1

        echo ""
        echo "qmailctl scripti'nin permission'i 755'e set ediliyor..."
        chmod 755 /var/qmail/bin/qmailctl
        echo ""

        sleep 1

        echo "qmailctl icin /usr/bin linki olusturuluyor..."
        ln -s /var/qmail/bin/qmailctl /usr/bin
        echo ""

        sleep 1

        echo "supervise qmail-send log dizini olusturuluyor..."
        mkdir -p /var/qmail/supervise/qmail-send/log
        echo ""

        sleep 1

        echo "supervise qmail-smtpd log dizini olusturuluyor..."
        mkdir -p /var/qmail/supervise/qmail-smtpd/log
        echo ""

        sleep 1

        echo "qmail-send/run dosyasi download ediliyor!"
        cd /var/qmail/supervise/qmail-send/
        fetch -q http://www.syslogs.org/qmail/qmail-send/run
        sleep 1
        echo "Done!"
        echo ""

        sleep 1


                                echo "qmail-send/run dosyasi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "qmail-send/run dosyasi yerinde mi ? "

                                if [ -f /var/qmail/supervise/qmail-send/run ]

                        then
                                        sleep 1
                                        echo "qmail-send/run .......... OK"

                        else

                                echo ""
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!"
                                echo "Ooopss..." && echo ""
                                echo "qmail-send/run dosyasi mevcut degil! Download sirasinda sorun"
                                echo "olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!" && echo ""
                                echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                echo "scriptini yeniden calistirin." && echo ""
                                echo "1 - http://www.syslogs.org/qmail/qmail-send/run adresine erisiminiz"
                                echo "oldugundan emin olun." && echo ""
                                echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                echo "qmail kurulumunu yeniden baslatin."
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                echo ""

                                sleep 1

                                echo ""
                                echo "Kurulum sonlandiriliyor!"
                                exit 0

                        fi


                                        echo ""
                                        echo "qmail-send/run dosyasinin ebati dogru mu ?"


                                        sleep 1


                        qsendrunNAME=/var/qmail/supervise/qmail-send/run
                        eval $(stat -s "$qsendrunNAME")


                        if [ ${st_size} =  29 ]

                        then

                                        echo "qmail-send/run size ...... Ok! (${st_size} bytes)"
                                        echo ""


                        else

                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmail-send/run dosyasinin size'i dogrulanamadi! Download sirasinda"
                                        echo "bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmail-send/run adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!"
                                        echo ""

                                        sleep 1

                                                echo ""
                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        sleep 1



        echo "qmail-send/log/run dosyasi download ediliyor!"
        cd /var/qmail/supervise/qmail-send/log
        fetch -q http://www.syslogs.org/qmail/qmail-send/log/run
        sleep 1
        echo "Done!"
        echo ""


        sleep 1

                                echo "qmail-send/log/run dosyasi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "qmail-send/log/run dosyasi yerinde mi ? "
                                echo ""

                                if [ -f /var/qmail/supervise/qmail-send/log/run ]

                        then
                                        sleep 1
                                        echo "qmail-send/log/run .......... OK"

                        else

                                echo ""
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!'''!!!!!!!!"
                                echo "Ooopss..." && echo ""
                                echo "qmail-send/log/run dosyasi mevcut degil! Download sirasinda sorun"
                                echo "olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!" && echo ""
                                echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                echo "scriptini yeniden calistirin." && echo ""
                                echo "1 - http://www.syslogs.org/qmail/qmail-send/log/run adresine erisiminiz"
                                echo "oldugundan emin olun." && echo ""
                                echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                echo "qmail kurulumunu yeniden baslatin."
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!'''!!!!!!"
                                echo ""

                                sleep 1

                                echo ""
                                echo "Kurulum sonlandiriliyor!"
                                exit 1

                        fi


                                        echo ""
                                        echo "qmail-send/log/run dosyasinin ebati dogru mu ?"
                                        echo ""

                                        sleep 1


                        qsendlogrunNAME=/var/qmail/supervise/qmail-send/log/run
                        eval $(stat -s "$qsendlogrunNAME")

                        if [ ${st_size} = 88 ]

                        then

                                        echo "qmail-send/log/run ...... Ok! (${st_size} bytes)"
                                        echo ""


                        else

                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmail-send/log/run dosyasinin size'i dogrulanamadi! Download sirasinda"
                                        echo "bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmail-send/log/run adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo ""

                                        sleep 1

                                                echo ""
                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        sleep 1

        echo "qmail-smtpd/run dosyasi download ediliyor..."
        cd /var/qmail/supervise/qmail-smtpd/
        fetch -q http://www.syslogs.org/qmail/qmail-smtpd/run
        sleep 1
        echo "Done!"
        echo ""

        sleep 1

                                echo "qmail-smtpd/run dosyasi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "qmail-smtpd/run dosyasi yerinde mi ? "


                                if [ -f /var/qmail/supervise/qmail-smtpd/run ]

                        then
                                        sleep 1
                                        echo "qmail-smtpd/run..........OK"

                        else

                                echo ""
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!'''!!!!!!!!"
                                echo "Ooopss..." && echo ""
                                echo "qmail-smtpd/run dosyasi mevcut degil! Download sirasinda sorun"
                                echo "olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!" && echo ""
                                echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                echo "scriptini yeniden calistirin." && echo ""
                                echo "1 - http://www.syslogs.org/qmail/qmail-smtpd/run adresine erisiminiz"
                                echo "oldugundan emin olun." && echo ""
                                echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                echo "qmail kurulumunu yeniden baslatin."
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!'''!!!!!!"
                                echo ""

                                sleep 1

                                echo ""
                                echo "Kurulum sonlandiriliyor!"
                                exit 0

                        fi


                                        echo ""
                                        echo "qmail-smtpd/run dosyasinin ebati dogru mu ?"
                                        echo ""

                                        sleep 1


                        qsmtpdrunNAME=/var/qmail/supervise/qmail-smtpd/run
                        eval $(stat -s "$qsmtpdrunNAME")

                        if [ ${st_size} = 751 ]

                        then

                                        echo " qmail-smtpd/run ...... Ok! (${st_size} bytes) "
                                        echo ""


                        else

                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmail-smtpd/run dosyasinin size'i dogrulanamadi! Download sirasinda"
                                        echo "bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmail-smtpd/run adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo ""

                                        sleep 1

                                                echo ""
                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        sleep 1

        echo "Concurrency Incoming Degeri 20'ye set ediliyor!"
        echo 20 > /var/qmail/control/concurrencyincoming
        chmod 644 /var/qmail/control/concurrencyincoming

        sleep 1

        echo ""
        echo "qmail-smtpd/log/run dosyasi download ediliyor..."
        cd /var/qmail/supervise/qmail-smtpd/log
        fetch -q http://www.syslogs.org/qmail/qmail-smtpd/log/run
        sleep 1
        echo "Done!"
        echo ""

        sleep 1


                                echo "qmail-smtpd/log/run betigi check ediliyor..."
                                echo ""

                        sleep 1

                                echo "qmail-smtpd/log/run dosyasi yerinde mi ? "


                                if [ -f /var/qmail/supervise/qmail-smtpd/log/run ]

                        then
                                        sleep 1
                                        echo "qmail-smtpd/log/run .......... OK"

                        else

                                echo ""
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!''''''!!!!!!!'''!!!!!!!!"
                                echo "Ooopss..." && echo ""
                                echo "qmail-smtpd/log/run dosyasi mevcut degil! Download sirasinda sorun"
                                echo "olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!" && echo ""
                                echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                echo "scriptini yeniden calistirin." && echo ""
                                echo "1 - http://www.syslogs.org/qmail/qmail-smtpd/log/run adresine erisiminiz"
                                echo "oldugundan emin olun." && echo ""
                                echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                echo "qmail kurulumunu yeniden baslatin."
                                echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''''!!!!!!'''!!!!!!"
                                echo ""

                                sleep 1

                                echo ""
                                echo "Kurulum sonlandiriliyor!"
                                exit 0

                        fi



                                        echo ""
                                        echo "qmail-smtpd/log/run betiginin ebati dogru mu ?"

                                        sleep 1


                        qsmtpdlogrunNAME=/var/qmail/supervise/qmail-smtpd/log/run
                        eval $(stat -s "$qsmtpdlogrunNAME")
                        if [ ${st_size} = 94 ]

                        then

                                        echo "qmail-smtpd/log/run ...... Ok! (${st_size} bytes) "
                                        echo ""


                        else
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo "Ooopss..." && echo ""
                                        echo "qmail-smtpd/log/run dosyasinin size'i dogrulanamadi! Download sirasinda"
                                        echo "bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz." && echo ""
                                        echo "Lutfen asagidaki adimlari izledikten sonra install-qmail.sh"
                                        echo "scriptini yeniden calistirin." && echo ""
                                        echo "1 - http://www.syslogs.org/qmail/qmail-smtpd/log/run adresine erisiminiz"
                                        echo "oldugundan emin olun." && echo ""
                                        echo "2- /qmail-install-scripts dizinindeki uninstall.sh scriptini"
                                        echo "calistirarak su ana kadar yapilan islemleri geri alin." && echo ""
                                        echo "3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak"
                                        echo "qmail kurulumunu yeniden baslatin."
                                        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''!!!!!!!!!!!!!!!!!!!"
                                        echo ""

                                        sleep 1

                                                echo ""
                                                echo "Kurulum sonlandiriliyor!"
                                                exit 0

                        fi


        echo "Conf dosyalari executable (755) yapiliyor..."
        chmod 755 /var/qmail/supervise/qmail-send/run
        chmod 755 /var/qmail/supervise/qmail-send/log/run
        chmod 755 /var/qmail/supervise/qmail-smtpd/run
        chmod 755 /var/qmail/supervise/qmail-smtpd/log/run
        echo ""

        sleep 1

        echo "smtpd log dizini olusturuluyor..."
        mkdir -p /var/log/qmail/smtpd
        echo ""

        sleep 1

        echo "smtpd log dizininin owner'i qmail'e set ediliyor..."
        chown qmaill /var/log/qmail /var/log/qmail/smtpd
        echo ""

        sleep 1

        echo "supervise dizini /service dizinine linkleniyor..."
        ln -s /var/qmail/supervise/qmail-send /var/qmail/supervise/qmail-smtpd /service
        echo ""

        sleep 1

        echo "localhost'a relay izni veriliyor..."
        echo ""
        sleep 1
        echo ""
        echo '127.:allow,RELAYCLIENT=""' >>/etc/tcp.smtp
        qmailctl cdb
        echo ""

        sleep 1

        sleep 1

        echo "Aliaslar ekleniyor..." && echo ""
        echo "[Root e-maillerinin gonderilecegi bir email adresi girin]:"
        read email

        echo $email > /var/qmail/alias/.qmail-root
        echo "Done!" && echo ""
        echo "root@sunucu.domain.com mailleri $email adresine gonderilecek." && echo ""

        sleep 1

        echo "[postmaster e-maillerinin gonderilecegi bir email adresi girin]:"
        read email

        echo $email > /var/qmail/alias/.qmail-postmaster
        echo "Done!" && echo ""
        echo "postmaster@sunucu.domain.com mailleri $email adresine gonderilecek." && echo ""

        sleep 1

        echo ".qmail-mailer-daemon ve .qmail-abuse .qmail-postmaster aliasina linkleniyor..."
        ln -s .qmail-postmaster /var/qmail/alias/.qmail-mailer-daemon
        ln -s .qmail-postmaster /var/qmail/alias/.qmail-abuse
        echo "Done!" && echo ""

        sleep 1

        echo "alias dosyalarinin permissionu 644'e set ediliyor..."
        chmod 644 /var/qmail/alias/.qmail-root /var/qmail/alias/.qmail-postmaster
        echo "Done!" && echo ""


        sleep 1

        echo ""
        echo "Qmail Configurasyonu Tamamlandi..."
        echo ""

        sleep 1

        exit 0

