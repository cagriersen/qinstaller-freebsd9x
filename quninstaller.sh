#!/usr/local/bin/bash

#!/bin/bash

echo ""
echo "##############################################################################################"
echo "UYARI: "
echo ""
echo " Bu script install-qmail.sh scripti ile yapilan qmail kurulumunu"
echo "uninstall etmektedir.!" && echo ""
echo "Uninstall islemi sirasinda qmail'e ait tum dizinler /qmail-backUP"
echo "dizinine tasinacaktir. Eger sisteminizde bulunan qmail, install-qmail.sh"
echo "scripti ile kurulmadi ise bu scripti calistirmayiniz. Aksi taktirde sitemde"
echo "problemler meydana gelebilir."
echo ""
echo "UNINSTALL SIRASINDA YAPILACAK ISLEMLER:"
echo ""
echo "1 - Eger yoksa /qmail-backUP isimli bir dizin olusturulacak!"
echo "2 - qmail stop edilecek!"
echo "3 - svscan icin eklenmis olan rc.local satiri kaldirilacak!"
echo "4 - /qmail-downloads dizini silinecek!"
echo "5 - /usr/local/src/netqmail-1.06 ve /usr/local/src/ucspi-tcp-0.88 dizinleri silinecek!"
echo "6 - /var/qmail dizini /qmail-backUP dizinine tasinacak!"
echo "7 - qmailctl dosyasina verilen /usr/bin altindaki link kaldirilacak!"
echo "7 - /var/log/qmail dizini /qmail-backUP dizinine tasinacak!"
echo "8 - /command dizini silinecek!"
echo "9 - /service dizini silinecek!"
echo "10 - /package dizini silinecek!"
echo "11 - /etc/tcp.smtp ve /etc/tcp.smtp.cdb dosyalari /qmail-backUP dizinine tasinacak!"
echo "12 - qmail grup ve kullanicilari sistemden silinecek (/etc/mail altindaki dizinler dahil)!"
echo "13 - /usr/local/bin altindaki daemontools linkleri silinecek"
echo "14 - svscan processi sonlandirilacak!"
echo "15 - tcpserver processi sonlandirilacak!"
echo "16 - qmail processleri sonlandirilacak!"
echo "17 - sendmail yeniden baslatilacak!"
echo "18 - sendmail startupa yeniden eklenecek"
echo ""
echo "Bu script, yeni kurulmus bir FreeBSD'ye gore hazirlanmistir. "
echo "Eger scripti calistiracaginiz FreeBSD halihazirda kullanimda ise"
echo "bu scriptin move edecegi dosya/dizinler arasinda sistem tarafindan"
echo "kullanilan dizin/dosyalar olabilir..."
echo "Eger boyle bir durum soz konusu ise scripti calistirmayiniz!"
echo ""
echo "##############################################################################################"
echo ""


function OnayAL() {
while true
do
echo -n "[Devam etmek istiyor musunuz ? <Evet/Hayir>] :"
read ONAY
case $ONAY in
Evet|evet|EVET) break ;;
Hayir|HAYIR|hayir|Hayir)
echo $ONAY dediginiz icin kurulum sonlandiriliyor...
exit
;;
*) echo Evet ya da Hayir demelisiniz.
esac
done
echo $ONAY dediniz, kuruluma devam ediyoruz.
}


OnayAL

       if [ -d /qmail-backUP ]

                then

                        echo "/qmail-backUP dizini mevcut. dosya/dizinler bu dizine tasinacak!" && echo ""

                else

                        echo "/qmail-backUP dizini mevcut degil!"
                        echo ""
                        mkdir /qmail-backUP
                        sleep 1
                        echo "Dizin olusturuldu. dosya/dizinler bu dizine tasinacak!"
                        echo ""

  	fi


                sleep 1
                        echo "Qmail durduruluyor..."
                        echo ""

                        qmailctl stop

                        echo ""

        sleep 2

        echo "/qmail-downloads dizini siliniyor..." && echo ""

        sleep 1

                rm -rf /qmail-downloads
        sleep 1

        echo "Done!" && echo ""

        sleep 1

                echo "/usr/local/src/netqmail ve ucspi dizinleri siliniyor...." && echo ""

                        rm -rf /usr/local/src/netqmail-1.06
                        rm -rf /usr/local/src/ucspi-tcp-0.88

        sleep 1

                echo "Done!" && echo ""

                sleep 1


                echo  "/var/qmail/ dizini backup dizinine tasiniyor..." && echo ""

                if [ -d /qmail-backUP/qmail ]

                then

                        rm -rf /qmail-backUP/qmail
                        mv -f /var/qmail/ /qmail-backUP/qmail/

                        echo "Done" && echo ""

                else

                        mv -f /var/qmail/ /qmail-backUP/qmail
                        sleep 1
                        echo "Done!" && echo ""

               fi


                sleep 1


                echo "qmailctl linki kaldiriliyor..." && echo ""

                        rm -f /usr/bin/qmailctl

                echo "Done!" && echo ""

                sleep 1


                echo "/var/log/qmail dizini backup dizinine tasiniyor..." && echo ""


                        if [ -d /qmail-backUP/var-log-qmail/ ]

                        then

                                rm -rf /qmail-backUP/var-log-qmail
                                mv -f /var/log/qmail/ /qmail-backUP/var-log-qmail/

                                echo "Done" && echo

                        else

                                mv -f /var/log/qmail/ /qmail-backUP/var-log-qmail/

                        sleep 1

                                echo "Done!" && echo ""

                        sleep 1

                        fi


                sleep 1

                echo "/command dizini siliniyor..." && echo

                        rm -rf /command
                sleep 1

                echo "Done!" && echo ""

                sleep 1


                echo "/service dizini siliniyor..." && echo ""

                        rm -rf /service

                sleep 1

                echo "Done!" && echo ""

                sleep 1


                echo "/package dizini siliniyor..." && echo ""

                        rm -rf /package

                sleep 1

                echo "Done!" && echo ""

                sleep 1

                        echo "/etc/tcp.smtp ve tcp.smtp.cdb kaldiriliyor..." && echo ""
                        mv -f /etc/tcp.smtp /qmail-backUP/
                        mv -f /etc/tcp.smtp.cdb /qmail-backUP/

                sleep 1

                        echo "Done!" && echo ""

                sleep 1

                echo "qmail grup ve kullanicilari kaldiriliyor!" && echo ""

                sleep 1


                pw user del alias
                pw user del qmaild
                pw user del qmaill
                pw user del qmailp
                pw user del qmailq
                pw user del qmailr
                pw user del qmails
                pw group del nofiles
                pw group del qmail

                echo "Done!" && echo ""

                sleep 1

                echo "/usr/local/bin altindaki daemontools linkleri kaldiriliyor..." && echo ""

                sleep 1

                        /usr/bin/find /usr/local/bin -type l | while read f; do if [ ! -e "$f" ]; then rm -f "$f"; fi; done

                echo "Done!" && echo ""

                sleep 2

                        echo "svscanboot icin eklenen rc.local girdisi kaldiriliyor...."

                        /usr/bin/grep -v 'svscanboot &' /etc/rc.local > /tmp/tempfile2 && mv -- /tmp/tempfile2 /etc/rc.local

                sleep 2

                        echo "Done!"
                        echo ""

                sleep 2

                        echo "svscan processi sonlandiriliyor..." && echo ""

                pid=`/bin/ps aux | /usr/bin/awk '/svscan/ && !/awk/ {print $2}'`;

                     if test "$pid" = "" ; then

                                echo "svscan zaten calismiyor!" && echo ""
                                sleep 1
                        else

                                kill $pid;
                                sleep 1
                                echo "Done!..." && echo ""

                        fi


                sleep 2

                        echo "tcpserver processi sonlandiriliyor..." && echo ""

                pid=`/bin/ps aux | /usr/bin/awk '/tcpserver/ && !/awk/ {print $2}'`;

                     if test "$pid" = "" ; then

				sleep 1

                                echo "tcpserver zaten calismiyor!" && echo ""
                                sleep 1
                        else

                                kill $pid;
                                sleep 1
                                echo "Done!..." && echo ""

                        fi


                sleep 2


                        echo "qmail processleri sonlandiriliyor..." && echo ""

                pid=`/bin/ps aux | /usr/bin/awk '/qmail/ && !/awk/ {print $2}'`;

                     if test "$pid" = "" ; then

                                echo "qmail zaten calismiyor!" && echo ""
                                sleep 1
                        else

                                kill $pid;
                                sleep 1
                                echo "Done!..." && echo ""

                        fi



                sleep 2

                        echo "sendmail starup'a ekleniyor..." && echo ""

                /usr/bin/grep -v '^sendmail_' /etc/rc.conf > /tmp/tempfile && mv -- /tmp/tempfile /etc/rc.conf

                sleep 1


                        echo "Sendmail baslatiliyor..." && echo ""

                sleep 1

		sh /etc/rc.d/sendmail onestart

                sleep 1

                        echo "Done" && echo ""

                sleep 1


                echo " Qmail uninstall tamamlandi...." && echo ""

                exit 0

